//
//  Assets.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-24.
//

import SwiftUI

struct Assets{
    static let starImage = Image("star")
    static let linkImage = Image("external-link")
}
