//
//  OnFirstAppearModifier.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-24.
//

import SwiftUI

import SwiftUI

struct OnFirstAppearModifier: ViewModifier {
    let perform:() -> Void
    @State private var firstTime: Bool = true
    
    func body(content: Content) -> some View {
        content
            .onAppear{
                if firstTime{
                    firstTime = false
                    self.perform()
                }
            }
    }
}

extension View {
   func onFirstAppear( perform: @escaping () -> Void ) -> some View {
     return self.modifier(OnFirstAppearModifier(perform: perform))
   }
}
