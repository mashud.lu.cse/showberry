//
//  HTTPClient.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-23.
//

import Foundation


enum NetworkError: LocalizedError {
    case badUrl
    case noData
    case decodingError
    case invalidStatusCode(statusCode: Int)
    case custom(error: Error)
    
    var errorDescription: String? {
        switch self {
        case .badUrl:
            return "URL isn't valid"
        case .noData:
            return "Response data is invalid"
        case .decodingError:
            return "Failed to decode"
        case .invalidStatusCode(_):
            return "Status code falls into the wrong range"
        case .custom(let error):
            return "Something went wrong \(error.localizedDescription)"
        }
    }
}

protocol HTTPClientImpl {
    func request<T: Codable>(endpoint: Endpoint,
                             type: T.Type) async throws -> T
}


class HTTPClient : HTTPClientImpl {
    
    func request<T>(endpoint: Endpoint, type: T.Type) async throws -> T where T: Codable {
        
        guard let url = endpoint.url else {
            throw NetworkError.badUrl
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse,
              (200...300) ~= response.statusCode else {
            let statusCode = (response as! HTTPURLResponse).statusCode
            throw NetworkError.invalidStatusCode(statusCode: statusCode)
        }
        
        guard let data = try? JSONDecoder().decode(T.self, from: data) else {
            throw NetworkError.decodingError
        }
        return data
        
    }
}
