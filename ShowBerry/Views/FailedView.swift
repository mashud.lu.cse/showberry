//
//  FailedView.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-23.
//

import SwiftUI

struct FailedView: View {
    var body: some View {
        Text("Something went wrong!!")
         .font(.largeTitle)
    }
}

struct FailedView_Previews: PreviewProvider {
    static var previews: some View {
        FailedView()
    }
}
