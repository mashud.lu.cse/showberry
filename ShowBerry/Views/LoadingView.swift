//
//  LoadingView.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-23.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        Text("Loading...")
         .font(.largeTitle)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
