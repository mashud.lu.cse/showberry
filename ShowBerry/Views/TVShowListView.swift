//
//  TVShowListView.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-23.
//

import SwiftUI

struct TVShowListView: View {
    let shows: [TVShowViewModel]
    var body: some View {
        List(shows, id: \.id) { show in
            NavigationLink(
                destination: TVShowDetailScreen(id: show.id),
                label: {
                    TVShowCell(item: show)
                })
        }
    }
}


/*
struct TVShowListView_Previews: PreviewProvider {
    static var previews: some View {
        TVShowListView()
    }
}*/
