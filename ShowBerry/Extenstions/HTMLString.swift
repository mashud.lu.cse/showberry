//
//  HTMLString.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-24.
//

import Foundation

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    
    /// Returns a string encoded in utf format from htm
    ///
    /// Here's an example
    ///
    ///     let movie = "<p><b>Under the Dome</b></p>"
    ///     print(movie.html2String)
    ///     // Prints "Under the Dome"
    ///
    /// - Returns: a string without html tags
    ///
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}
