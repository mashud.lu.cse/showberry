//
//  ViewExtension.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-24.
//

import SwiftUI

extension View {
    
    /// Wrapped view inside a navigation view
    ///
    /// - Returns: Navigation view 
    ///
    func embedNavigationView() ->  some View {
        return NavigationView{self}
    }
}
