//
//  ShowBerryApp.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-23.
//

import SwiftUI

@main
struct ShowBerryApp: App {
    var body: some Scene {
        WindowGroup {
            TVShowListScreen()
        }
    }
}
