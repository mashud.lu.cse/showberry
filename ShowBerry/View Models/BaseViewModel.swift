//
//  BaseViewModel.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-23.
//

import Foundation

enum LoadingState {
    case loading, success, failed, none
}

class BaseViewModel: ObservableObject {
    @Published var loadingState: LoadingState = .none
}
