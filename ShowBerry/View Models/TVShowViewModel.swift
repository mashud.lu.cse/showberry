//
//  TVShowViewModel.swift
//  ShowBerry
//
//  Created by Mashud Jaman on 2022-12-23.
//

import Foundation

struct TVShowViewModel {
    
    var show: TVShow
    
    var id: Int {
        show.id
    }
    
    var image: String {
        show.image?.medium ?? ""
    }
    
    var title: String {
        show.name
    }
    
    var genres: String {
        show.genres?.joined(separator: " | ") ?? ""
    }
}
