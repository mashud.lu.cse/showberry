# ShowBerry

ShowBerry is a simple application to search TV Shows. 

## Overview

This App consists of two screens. The first screen contains a list of shows and a search view. Search view can be used for searching desired TV shows. All list items are clickable, and the user can see the show details by clicking the item. The second screen is the detail view screen, where the user can see the details, including short descriptions, ratings, language, genre, etc. There might be two links (if available) to see more details of the show from the TVMaze website or its original website.

The App is built with MVVM Architecture with Apple's new declarative SwiftUI.
 
The API ​http://www.tvmaze.com/api​ is used to get the list of shows. Network calls are implemented using URLSession with async wait pattern. There is also an ImageLoader to load images from URLs asynchronously.

The App is built with protocol-oriented design pattern so that it can be extended for Unit testing. It is also built keeping the SOLID design principle.

## Framework, Architecture, Pattern
- Swift <br/>
- SwiftUI <br/>
- MVVM<br/>
- Combine<br/>
- Protocol Oriented Programming <br/>
- @MainActor<br/>
- ViewModifier<br/>
- Asyc/Await<br/>
- Enum<br/>
 

## Time Spent (Approximate)


Git initialization and project setup - 20 mins <br/>
Investigate API - 20 mins <br/>
Design Models - 10 mins <br/>
Implemted HTTP Client  - 30 mins <br/>
Design Screens and Components - 1 hr 30 mins <br/>
Reviewing, Debugging, Modification - 40 mins <br/>
Write Readme - 15 mins <br/>


## ToDo 
-Unit Testing

