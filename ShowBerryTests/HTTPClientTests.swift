//
//  HTTPClientTests.swift
//  ShowBerryTests
//
//  Created by Mashud Jaman on 2022-12-26.
//

import XCTest
@testable import ShowBerry

final class HTTPClientTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_shouldThrow_BadURL() async throws {
        let endpoint = Endpoint.showSearch(title: "Bad Url")
        let client = HTTPClient()
        
        do {
            _ = try await client.request(endpoint: endpoint, type: SearchItem.self)
            XCTFail("This call should throw bad url error.")
        }
        catch let error {
            XCTAssertTrue(error is ShowBerry.NetworkError)
            let error = error as? ShowBerry.NetworkError
            XCTAssertEqual(error?.errorDescription, NetworkError.badUrl.errorDescription)
        }
        
    }
    
    func test_shouldThrow_DecodingError() async throws {
        let endpoint = Endpoint.showSearch(title: "Batman")
        let client = HTTPClient()
        
        do {
            _ = try await client.request(endpoint: endpoint, type: SearchItem.self)
            XCTFail("This call should throw decoding error.")
        }
        catch let error {
            XCTAssertTrue(error is ShowBerry.NetworkError)
            let error = error as? ShowBerry.NetworkError
            XCTAssertEqual(error?.errorDescription, NetworkError.decodingError.errorDescription)
        }
        
    }
    
    func test_ResponseData_isCorrect() async throws {
        let endpoint = Endpoint.showDetail(id: 1)
        let client = HTTPClient()
        
        do {
            let tvShow = try await client.request(endpoint: endpoint, type: TVShow.self)
            XCTAssertEqual(tvShow.id, 1)
        }
        catch {
            XCTFail("This call shouldn't throw any error.")
        }
        
    }


}
