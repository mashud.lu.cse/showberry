//
//  JSONMapper.swift
//  ShowBerryTests
//
//  Created by Mashud Jaman on 2022-12-26.
//

import Foundation

struct JSONMapper {
    //MARK:- for mapping Stub Data
    static func decode<T: Decodable>(data: Data, type: T.Type) throws -> T {
        
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
        
    }
}
